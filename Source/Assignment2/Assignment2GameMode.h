// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "Assignment2HUD.h"
#include "Assignment2GameMode.generated.h"

UCLASS(minimalapi)
class AAssignment2GameMode : public AGameModeBase
{
	GENERATED_BODY()


private:
	TSubclassOf<UAssignment2HUD> PlayerHudClass;
	UAssignment2HUD* PlayerHud;

	bool Paused;
public:
	AAssignment2GameMode();

	UFUNCTION(BlueprintCallable, Category = "Functions")
		void SetGamePause(bool state);
	bool GetGamePaused();

	virtual void BeginPlay() override;
};



