// Fill out your copyright notice in the Description page of Project Settings.

#include "Assignment2.h"
#include "FuseKey.h"


// Sets default values
AFuseKey::AFuseKey()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFuseKey::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFuseKey::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

