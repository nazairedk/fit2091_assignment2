// Fill out your copyright notice in the Description page of Project Settings.

#include "Assignment2.h"
#include "APickup.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AAPickup::AAPickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	bReplicates = true;
	bReplicateMovement = true;
	GetStaticMeshComponent()->SetMobility(EComponentMobility::Movable);
	GetStaticMeshComponent()->SetSimulatePhysics(true);
}

void AAPickup::OnRep_IsActive() {
	// test
}

// Called when the game starts or when spawned
void AAPickup::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAPickup::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AAPickup::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AAPickup, bIsActive);
	
}

bool AAPickup::isActive() {
	return bIsActive;
}

void AAPickup::setActive(bool NewState) {
	if (Role == ROLE_Authority) {
		bIsActive = NewState;
	}
}