// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Assignment2HUD.generated.h"
/**
 * 
 */
UCLASS()
class ASSIGNMENT2_API UAssignment2HUD : public UUserWidget
{
	GENERATED_UCLASS_BODY()

public:
		UAssignment2HUD();
		void AddToViewport();
		
		UFUNCTION(BlueprintImplementableEvent, Category = "Gamemode")
			void goStartScreen();

		UFUNCTION(BlueprintImplementableEvent, Category = "Gamemode")
			void updateHealth(float health);

		UFUNCTION(BlueprintImplementableEvent, Category = "Gamemode")
			void goEndScreen();

		UFUNCTION(BlueprintImplementableEvent, Category = "Gamemode")
			void goPauseScreen();

};
