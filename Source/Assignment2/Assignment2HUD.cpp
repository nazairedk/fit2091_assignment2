// Fill out your copyright notice in the Description page of Project Settings.

#include "Assignment2.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Assignment2HUD.h"


UAssignment2HUD::UAssignment2HUD(const FObjectInitializer& ObjectInitializer) :Super(ObjectInitializer) {
	
}

void UAssignment2HUD::AddToViewport() {
	Super::AddToViewport();
}
