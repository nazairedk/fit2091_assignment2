// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "Assignment2.h"
#include "Assignment2GameMode.h"
#include "Assignment2Character.h"
#include "Assignment2PlayerController.h"
#include "Assignment2HUD.h"

AAssignment2GameMode::AAssignment2GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UAssignment2HUD> PlayerHudBPClass(TEXT("/Game/Assignment2HUD"));
	if (PlayerHudBPClass.Class != NULL) {
		PlayerHudClass = PlayerHudBPClass.Class;
	}
	else {
		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Could not find class!"));
	}

	Paused = false;
}

void AAssignment2GameMode::SetGamePause(bool state) {
	if (Role == ROLE_Authority) {
		AAssignment2PlayerController* const MyPlayer = Cast<AAssignment2PlayerController>(GEngine->GetFirstLocalPlayerController(GetWorld()));
		MyPlayer->SetPause(state);
		Paused = state;

	}
	
	if (state) {
		AAssignment2PlayerController* const MyPlayer = Cast<AAssignment2PlayerController>(GEngine->GetFirstLocalPlayerController(GetWorld()));
		MyPlayer->GetPlayerHud()->goPauseScreen();
	}
}

bool AAssignment2GameMode::GetGamePaused() {
	return Paused;
}

void AAssignment2GameMode::BeginPlay() {
	Super::BeginPlay();

}

