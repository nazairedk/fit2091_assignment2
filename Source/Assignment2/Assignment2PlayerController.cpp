// Fill out your copyright notice in the Description page of Project Settings.

#include "Assignment2.h"
#include "Assignment2HUD.h"
#include "Assignment2Character.h"
#include "Assignment2PlayerController.h"


AAssignment2PlayerController::AAssignment2PlayerController() {

	static ConstructorHelpers::FClassFinder<UAssignment2HUD> PlayerHudBPClass(TEXT("/Game/Assignment2HUD"));
	if (PlayerHudBPClass.Class != NULL) {
		PlayerHudClass = PlayerHudBPClass.Class;
	}
	else {
		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Could not find class!"));
	}
}


void AAssignment2PlayerController::BeginPlay() {
	Super::BeginPlay();

	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("BeginPlay Controller"));

	if (PlayerHudClass && IsLocalPlayerController()) {
		PlayerHud = CreateWidget<UAssignment2HUD>(GetWorld(), PlayerHudClass);
		
		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("PlayerHudClass"));

		if (PlayerHud) {
			PlayerHud->AddToViewport();
			PlayerHud->goStartScreen();

			bShowMouseCursor = true;
			bEnableClickEvents = true;
			bEnableMouseOverEvents = true;

			if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Added to viewport!"));
		}

	}

}

UAssignment2HUD* AAssignment2PlayerController::GetPlayerHud() {
	return PlayerHud;
}

void AAssignment2PlayerController::PlayerTick(float dt) {
	Super::PlayerTick(dt);
	if (IsLocalPlayerController() && PlayerHud) {
		AAssignment2Character* pawn = Cast<AAssignment2Character>(GetControlledPawn());
		PlayerHud->updateHealth(pawn->GetHealth());
	}
}