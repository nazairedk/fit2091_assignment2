// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "Assignment2HUD.h"
#include "Assignment2PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENT2_API AAssignment2PlayerController : public APlayerController
{
	GENERATED_BODY()
	
private:
	TSubclassOf<UAssignment2HUD> PlayerHudClass;
	UAssignment2HUD* PlayerHud;

public:
	AAssignment2PlayerController();
	
	virtual void BeginPlay() override;
	virtual void PlayerTick(float dt) override;

	UAssignment2HUD* GetPlayerHud();
};
