// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Assignment2 : ModuleRules
{
	public Assignment2(TargetInfo Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "UMG", "Slate", "Core", "SlateCore", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
