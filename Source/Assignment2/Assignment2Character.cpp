// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "Assignment2.h"
#include "Assignment2GameMode.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "Assignment2Character.h"
#include "Runtime/UMG/Public/Blueprint/WidgetLayoutLibrary.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "APickup.h"
#include "Interactable.h"
#include "Assignment2HUD.h"

//////////////////////////////////////////////////////////////////////////
// AAssignment2Character

AAssignment2Character::AAssignment2Character()
{
	SetActorTickEnabled(true);

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	static ConstructorHelpers::FClassFinder<UAssignment2HUD> PlayerHudBPClass(TEXT("/Game/Assignment2HUD"));
	if (PlayerHudBPClass.Class != NULL) {
		PlayerHudClass = PlayerHudBPClass.Class;
	}
	else {
		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Could not find class!"));
	}

	// set player health to full
	health = 1.0f;
}

//////////////////////////////////////////////////////////////////////////
// Input

void AAssignment2Character::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AAssignment2Character::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AAssignment2Character::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AAssignment2Character::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AAssignment2Character::LookUpAtRate);
	
	PlayerInputComponent->BindAction("Pickup", IE_Pressed, this, &AAssignment2Character::Pickup);
	PlayerInputComponent->BindAction("Pause", IE_Pressed, this, &AAssignment2Character::PausePressed);
	// handle touch devices
	// PlayerInputComponent->BindTouch(IE_Pressed, this, &AAssignment2Character::TouchStarted);
	// PlayerInputComponent->BindTouch(IE_Released, this, &AAssignment2Character::TouchStopped);

	// VR headset functionality
	// PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AAssignment2Character::OnResetVR);
}

void AAssignment2Character::PausePressed() {
	AAssignment2GameMode* GM = (AAssignment2GameMode*) GetWorld()->GetAuthGameMode();

	if (Role == ROLE_Authority) {
		bool state = GM->GetGamePaused();
		GM->SetGamePause(!state);
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Triggered Pause"));
	}
}

void AAssignment2Character::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AAssignment2Character::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void AAssignment2Character::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void AAssignment2Character::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AAssignment2Character::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AAssignment2Character::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AAssignment2Character::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AAssignment2Character::BeginPlay() {
	Super::BeginPlay();
}

void AAssignment2Character::Pickup() {
	CallMyTrace();
}

FString AAssignment2Character::MyRole() {
	if (Role == ROLE_Authority) {
		return TEXT("Server");
	}
	else {
		return TEXT("Client");
	}
}

float AAssignment2Character::GetHealth() {
	return health;
}

bool AAssignment2Character::Trace(
	UWorld* World,
	TArray<AActor*>& ActorsToIgnore,
	const FVector& Start,
	const FVector& End,
	FHitResult& HitOut,
	ECollisionChannel CollisionChannel = ECC_Pawn,
	bool ReturnPhysMat = false
	) {
	
	// The World parameter refers to our game world (map/level) 
	// If there is no World, abort
	if (!World)
	{
		return false;
	}

	// Set up our TraceParams object
	FCollisionQueryParams TraceParams(FName(TEXT("My Trace")), true, ActorsToIgnore[0]);

	// Should we simple or complex collision?
	TraceParams.bTraceComplex = true;

	// We don't need Physics materials 
	TraceParams.bReturnPhysicalMaterial = ReturnPhysMat;

	// Add our ActorsToIgnore
	TraceParams.AddIgnoredActors(ActorsToIgnore);

	// When we're debugging it is really useful to see where our trace is in the world
	// We can use World->DebugDrawTraceTag to tell Unreal to draw debug lines for our trace
	// (remove these lines to remove the debug - or better create a debug switch!)
	const FName TraceTag("MyTraceTag");
	World->DebugDrawTraceTag = TraceTag;
	TraceParams.TraceTag = TraceTag;


	// Force clear the HitData which contains our results
	HitOut = FHitResult(ForceInit);

	// Perform our trace
	World->LineTraceSingleByChannel
		(
			HitOut,		//result
			Start,	//start
			End, //end
			CollisionChannel, //collision channel
			TraceParams
		);

	// If we hit an actor, return true
	return (HitOut.GetActor() != NULL);
}

void AAssignment2Character::ClearPickupInfo() {
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("RemovedPICKUP"));
	pickedUpActor = NULL;
}

void AAssignment2Character::CallMyTrace()
{
	// Get the location of the camera (where we are looking from) and the direction we are looking in
	FVector ViewLocation;
	FRotator ViewRotation;
	GetActorEyesViewPoint(ViewLocation, ViewRotation);
	const FVector Start = ViewLocation;
	const FVector ForwardVector = FollowCamera->GetForwardVector();

	// How for in front of our character do we want our trace to extend?
	// ForwardVector is a unit vector, so we multiply by the desired distance
	const FVector End = Start + ForwardVector * 512;

	// Force clear the HitData which contains our results
	FHitResult HitData(ForceInit);

	// What Actors do we want our trace to Ignore?
	TArray<AActor*> ActorsToIgnore;

	//Ignore the player character - so you don't hit yourself!
	ActorsToIgnore.Add(this);
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("FiredTrace"));
	// Call our Trace() function with the paramaters we have set up
	// If it Hits anything
	if (Trace(GetWorld(), ActorsToIgnore, Start, End, HitData, ECC_Visibility, false ))
	{
		// Process our HitData
		if (HitData.GetActor())
		{
			UE_LOG(LogClass, Warning, TEXT("This a testing statement. %s"), *HitData.GetActor()->GetName());
			ProcessTraceHit(HitData);

		}
		else
		{
			// The trace did not return an Actor
			// An error has occurred
			// Record a message in the error log
		}
	}
	else
	{
		// We did not hit an Actor
		ClearPickupInfo();

	}

}

void AAssignment2Character::ProcessTraceHit(FHitResult& HitOut)
{

	// Cast the actor to APickup
	AAPickup* const TestPickup = Cast<AAPickup>(HitOut.GetActor());

	if (TestPickup)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, "APICKUP!!");
		// Keep a pointer to the Pickup
		pickedUpActor = TestPickup;
		pickedUpActor->GetStaticMeshComponent()->SetSimulatePhysics(false);
		
		// Set a local variable of the PickupName for the HUD
		//UE_LOG(LogClass, Warning, TEXT("PickupName: %s"), *TestPickup->GetPickupName());
		//PickupName = TestPickup->GetPickupName();

		// Set a local variable of the PickupDisplayText for the HUD
		//UE_LOG(LogClass, Warning, TEXT("PickupDisplayText: %s"), *TestPickup->GetPickupDisplayText());
		//PickupDisplayText = TestPickup->GetPickupDisplayText();
		//PickupFound = true;
	}
	else
	{
		//UE_LOG(LogClass, Warning, TEXT("TestPickup is NOT a Pickup!"));
		ClearPickupInfo();
	}

	AInteractable* const interactableActor = Cast<AInteractable>(HitOut.GetActor());
	if (interactableActor) {
		AInteractable* const object = Cast<AInteractable>(HitOut.GetActor());

		object->Interact();
	}
}

void AAssignment2Character::consumeHealthPack() {
	health = health + 0.1f;

	if (health > 1.0f) {
		health = 1.0f;
	}
}
// Called every frame
void AAssignment2Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	// Call your functions
	if (pickedUpActor) {
		FVector currentLocation = pickedUpActor->GetActorLocation();

		FVector forward = GetActorForwardVector();
		forward.Normalize();

		FVector infrontOf = GetActorLocation() + (forward * 100.0f);

		FVector hoverLocation = FMath::Lerp(currentLocation, infrontOf, 4.0f * DeltaTime);

		pickedUpActor->SetActorLocation(hoverLocation);

	}

	if (health > 0) {
		health = health - 0.0001;
	}

}