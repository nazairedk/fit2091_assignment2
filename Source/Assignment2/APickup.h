// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/StaticMeshActor.h"
#include "APickup.generated.h"

UCLASS()
class ASSIGNMENT2_API AAPickup : public AStaticMeshActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAPickup();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(ReplicatedUsing = OnRep_IsActive)
		bool bIsActive;

	UFUNCTION(BlueprintCallable, Category = "Test")
		virtual void OnRep_IsActive();

	// Networking Stuff
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	UFUNCTION(BlueprintPure, Category = "Test")
		bool isActive();

	UFUNCTION(BlueprintCallable, Category = "Test")
		void setActive(bool NewState);
	
};
