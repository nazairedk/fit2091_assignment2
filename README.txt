FIT2097 - Assignment 2
Dylan Kay - 28823168


My game involves two players who have to pass puzzles before they die. For my extra functionality I implemented my physics "carrying" system in C++.
This system works by a player pressing 'E' on an object that is of type 'APickup', the player then carries this object with them.
For example the key and fuse are items the player must pickup to unlock the next door.

I was unable to complete the final stages of the assignment - the end screen.

Known bugs: The player can be slown down by the carried item, seems to be because of collisions.

